#include "MandelbrotEXC.hpp"

#include <cstdio>
#include <cstdlib>
#include <sstream>

#include <CL/cl.h>
#include <bbque/utils/utility.h>


/*************************************************************/
/* Setup Barbeque logging                                    */
/*************************************************************/
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "aem.mandelbrot"
#undef  BBQUE_LOG_UID
#define BBQUE_LOG_UID GetUniqueID_String()


/*************************************************************/
/* Macros for logging RTLib calls                            */
/*************************************************************/

#define MSG_INF( format, ... ) \
	fprintf(stderr, FI("[INF] " format "\n"), ##__VA_ARGS__);

#define MSG_ERR( format, ... ) \
	fprintf(stderr, FE("[ERR] " format "\n"), ##__VA_ARGS__);

#define METHOD_ENTER(function) \
	fprintf(stderr, FI("[INFO] %s called\n"), function)

#define METHOD_EXIT(function) \
	fprintf(stderr, FI("[INFO] %s done\n"), function)



MandelbrotEXC::MandelbrotEXC(std::string const & name, std::string const & recipe,
		RTLIB_Services_t *rtlib) :
	BbqueEXC(name, recipe, rtlib),
	clMandelbrot("OpenCL Mandelbrot fractal")
{
	if (clMandelbrot.initialize() != SDK_SUCCESS)
	{
		MSG_ERR("Method clMandelbrot_initialize failed");
		exit(EXIT_FAILURE);
	}
}

MandelbrotEXC::~MandelbrotEXC()
{

}

int MandelbrotEXC::parseCommandLine(int argc, char *argv[])
{
	if (clMandelbrot.parseCommandLine(argc, argv) != SDK_SUCCESS)
	{
		MSG_ERR("Method clMandelbrot_parseCommandLine failed");
		return SDK_FAILURE;
	}
	return SDK_SUCCESS;
}

bool MandelbrotEXC::isDumpBinaryEnabled()
{
	return clMandelbrot.isDumpBinaryEnabled();
}

int MandelbrotEXC::genBinaryImage()
{
	if (clMandelbrot.genBinaryImage() != SDK_SUCCESS)
	{
		MSG_ERR("Method clMandelbrot_genBinaryImage failed");
		return SDK_FAILURE;
	}
	return SDK_SUCCESS;
}

int MandelbrotEXC::cleanup()
{
	if (clMandelbrot.cleanup() != SDK_SUCCESS)
	{
		MSG_ERR("Method clMandelbrot_cleanup failed");
		return SDK_FAILURE;
	}
	return SDK_SUCCESS;
}

Mandelbrot *MandelbrotEXC::getPointer()
{
	return &clMandelbrot;
}

RTLIB_ExitCode_t MandelbrotEXC::onSetup()
{
	METHOD_ENTER("onSetup");

	if (clMandelbrot.getAwm() >= 0) {
		RTLIB_Constraint_t constr_set = {
			clMandelbrot.getAwm(),
			CONSTRAINT_ADD,
			UPPER_BOUND
		};
		SetConstraints(&constr_set, 1);
	}

	METHOD_EXIT("onSetup");
	return RTLIB_OK;
}

bool MandelbrotEXC::oclDeviceChanged()
{
	bool ret = false;

	char *device_name_str = NULL;
	size_t device_name_size = 0;

	cl_int status = 0;
	cl_device_id devId;
	cl_platform_id pltId;

	status = clGetPlatformIDs(1, &pltId, NULL);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetPlatformIDs failed");
		exit(EXIT_FAILURE);
	}

	status = clGetDeviceIDs(pltId, CL_DEVICE_TYPE_ALL, 1, &devId, NULL);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetDeviceIDs failed");
		exit(EXIT_FAILURE);
	}

	status = clGetDeviceInfo(devId, CL_DEVICE_NAME, 0, NULL, &device_name_size);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetDeviceInfo failed");
		exit(EXIT_FAILURE);
	}

	device_name_str = (char *)malloc(device_name_size+1);
	if (device_name_str == NULL) {
		MSG_ERR("Memory allocation failed");
		exit(EXIT_FAILURE);
	}

	status = clGetDeviceInfo(devId, CL_DEVICE_NAME, device_name_size, device_name_str, &device_name_size);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetDeviceInfo failed");
		exit(EXIT_FAILURE);
	}

	device_name_str[device_name_size] = '\0';
	if (device_name.compare(device_name_str) != 0) {
		device_name = device_name_str;
		ret = true;
	}

	free ((void *)device_name_str);

	return ret;
}

RTLIB_ExitCode_t MandelbrotEXC::onConfigure(uint8_t awm_id)
{
	if (!oclDeviceChanged()) {
		MSG_INF("Reconfiguring quota on the same device [%s]", device_name.c_str());
		return RTLIB_OK;
	}
	MSG_INF("OpenCL device has changed [%s]", device_name.c_str());


	METHOD_ENTER("onConfigure");

	if ( (Cycles() > 0) && (clMandelbrot.cleanup() != SDK_SUCCESS) )
	{
		MSG_ERR("Method clMandelbrot_cleanup failed");
		return RTLIB_ERROR;
	}

	if (clMandelbrot.setup() != SDK_SUCCESS)
	{
		MSG_ERR("Method clMandelbrot_setup failed");
		return RTLIB_ERROR;
	}

	METHOD_EXIT("onConfigure");
	return RTLIB_OK;
}

RTLIB_ExitCode_t MandelbrotEXC::onRun()
{
	if ( Cycles() >= clMandelbrot.getIterations() )
	{
		return RTLIB_EXC_WORKLOAD_NONE;
	}

	METHOD_ENTER("onRun");
	if (clMandelbrot.runCLKernels()!=SDK_SUCCESS)
	{
		MSG_ERR("Method clMandelbrot_runCLKernels failed");
		Terminate();
		return RTLIB_ERROR;
	}
	METHOD_EXIT("onRun");

	return RTLIB_OK;
}

RTLIB_ExitCode_t MandelbrotEXC::onMonitor()
{
	return RTLIB_OK;
}

RTLIB_ExitCode_t MandelbrotEXC::onRelease()
{
	METHOD_ENTER("onRelease");

	if (clMandelbrot.verifyResults()!=SDK_SUCCESS)
	{
		MSG_ERR("Method clMandelbrot_verifyResults failed");
		return RTLIB_ERROR;
	}

//	if (clMandelbrot.cleanup() != SDK_SUCCESS)
//	{
//		MSG_ERR("Method clMandelbrot_cleanup failed");
//		return RTLIB_ERROR;
//	}

	METHOD_EXIT("onRelease");

	return RTLIB_OK;
}

RTLIB_ExitCode_t MandelbrotEXC::onSuspend()
{
	METHOD_ENTER("onSuspend");

	fprintf(stderr, "\n");
	fprintf(stderr, "*** Execution context suspended ***\n");
	fprintf(stderr, "\n");

	METHOD_EXIT("onSuspend");

	return RTLIB_OK;
}

