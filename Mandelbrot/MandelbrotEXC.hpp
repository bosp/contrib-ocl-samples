#ifndef MANDELBROT_EXC_H
#define MANDELBROT_EXC_H

#include <bbque/bbque_exc.h>

#include "Mandelbrot.hpp"

using bbque::rtlib::BbqueEXC;


class MandelbrotEXC : public BbqueEXC
{
public:
	MandelbrotEXC(std::string const & name, std::string const & recipe,
		RTLIB_Services_t *rtlib);
	~MandelbrotEXC();

	int parseCommandLine(int argc, char *argv[]);
	bool isDumpBinaryEnabled();
	int genBinaryImage();
	int cleanup();

	Mandelbrot * getPointer();

private:
	bool oclDeviceChanged();

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(uint8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onRelease();
	RTLIB_ExitCode_t onSuspend();

	Mandelbrot clMandelbrot;

	std::string device_name;
};

#endif /* define(MANDELBROT_EXC_H) */
