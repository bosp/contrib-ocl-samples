
ifdef CONFIG_CONTRIB_OCL_MANDELBROT

# Targets provided by this project
.PHONY: ocl-mandelbrot clean_ocl-mandelbrot

# Add this to the "ocl-samples" target
ocl-samples: ocl-mandelbrot
clean_ocl-samples: clean_ocl-mandelbrot

MODULE_CONTRIB_OCL_MANDELBROT=contrib/user/ocl-samples/Mandelbrot

ocl-mandelbrot: ocl-sdkutil
	@echo
	@echo "==== Building MandelbrotOCL ($(BUILD_TYPE)) ===="
	@[ -d $(MODULE_CONTRIB_OCL_MANDELBROT)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_OCL_MANDELBROT)/build/$(BUILD_TYPE) || \
		exit 1
	cd $(MODULE_CONTRIB_OCL_MANDELBROT)/build/$(BUILD_TYPE) && \
		CXX=$(CXX) cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_OCL_MANDELBROT)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_ocl-mandelbrot:
	@echo
	@echo "==== Clean-up MandelbrotOCL ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/bbque-ocl-mandelbrot ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/BbqOCLMandelbrot.recipe; \
		rm -f $(BUILD_DIR)/usr/bin/bbque-ocl-mandelbrot; \
		rm -f $(BUILD_DIR)/usr/bin/Mandelbrot_Kernels.cl;
	@rm -rf $(MODULE_CONTRIB_OCL_MANDELBROT)/build
	@echo

run-ocl-mandelbrot: ocl-mandelbrot
	@echo
	@echo "==== Running MandelbrotOCL ===="
	$(MODULE_CONTRIB_OCL_MANDELBROT)/run_app.sh $(BUILD_DIR)/usr/bin

else # CONFIG_CONTRIB_OCL_MANDELBROT

ocl-mandelbrot:
	$(warning contrib/bbque-ocl-mandelbrot module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_OCL_MANDELBROT

