#!/bin/bash

PREFIX=$1

${PREFIX}/bbque-ocl-mandelbrot \
--device all \
--quiet \
--iterations 1000 \
--width 1024 \
--height 1024
