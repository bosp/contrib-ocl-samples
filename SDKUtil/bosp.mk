
ifdef CONFIG_CONTRIB_SDKUTIL

# Targets provided by this project
.PHONY: ocl-sdkutil clean_ocl-sdkutil

# Add this to the "ocl-samples" target
ocl-samples: ocl-sdkutil
clean_ocl-samples: clean_ocl-sdkutil

MODULE_CONTRIB_OCL_SDKUTIL=contrib/user/ocl-samples/SDKUtil

ocl-sdkutil:
	@echo
	@echo "==== Building SDKUtil library ($(BUILD_TYPE)) ===="
	@[ -d $(MODULE_CONTRIB_OCL_SDKUTIL)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_OCL_SDKUTIL)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_CONTRIB_OCL_SDKUTIL)/build/$(BUILD_TYPE) && \
		CXX=$(CXX) cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_OCL_SDKUTIL)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) || \
		exit 1

clean_ocl-sdkutil:
	@echo
	@echo "==== Clean-up SDKUtil library ===="
	@rm -rf $(MODULE_CONTRIB_OCL_SDKUTIL)/build
	@echo

else # CONFIG_CONTRIB_SDKUTIL

ocl-sdkutil:
	$(warning contrib/bbque-sdkutil module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_SDKUTIL

