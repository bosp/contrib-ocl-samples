#!/bin/bash

APPS=( "FluidSimulation2D" "Mandelbrot" "MonteCarloAsianDP" "NBody" )

RECIPES=( "BbqOCLFluidSimulation2D" "BbqOCLMandelbrot" "BbqOCLMonteCarloAsianDP" "BbqOCLNBody" )

MOST_PATH="/usr/local/most-pdm"


for i in `seq 0 $(echo "${#APPS[@]} - 1" | bc)`
do
	APP_NAME=${APPS[$i]}
	APP_RECIPE=${RECIPES[$i]}

	# Configure DSE
	cat design_space.xml.in | sed \
		-e "s?@app_name@?${APP_NAME}?g" \
		-e "s?@app_recipe@?${APP_RECIPE}?g" > design_space.xml

	# Install application executable
	if [ -L "run.sh" ]; then
		unlink "run_app.sh"
	fi
	ln -s ../${APP_NAME}/run_app.sh

	cp databases/full_${APP_NAME}.db databases/full.db

	# Run DSE
	${MOST_PATH}/bin/most -x design_space.xml -f scripts/most/pareto.scr

	# Save results
	mv databases/pareto.csv databases/pareto_${APP_NAME}.csv

	# Cleanup
	unlink "run_app.sh"
	rm -f design_space.xml
	rm -f databases/full.db
done

# MOST cleanup
rm -f check
rm -f normal_input
rm -f normal_output
rm -f rand_input
rm -f rand_output

exit 0
