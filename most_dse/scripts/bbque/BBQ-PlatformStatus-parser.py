# coding=utf-8

import argparse
import csv
import re

max_window = 2

metrics = [ "adapter_gpu_id", "load", "temperature", "core_frequency", "mem_frequency", "fanspeed", "voltage", "performance_level", "power_state" ]


parser = argparse.ArgumentParser(description='Compute average of selected column from BBQ-PlatformStatus-GPU file')
parser.add_argument('-f','--file', help='BBQ-PlatformStatus-GPU file', required=True, type=str, dest='filename')
parser.add_argument('-c','--col', help='GPU metric column (1-NumColumns)', required=True, type=int, dest='column')


args = parser.parse_args()

col = args.column-1


# Load the BBQ-PlatformStatus-GPU file
log = list(csv.reader(open(args.filename, "rb"), delimiter=' ', skipinitialspace=True))

# Parse the BBQ-PlatformStatus-GPU file and compute average of selected metric
start = False
window = 0
metric_sum = 0
count = 0

for line in log:

	# Skip comments
	if re.search("^#", line[0]):
		continue

	# GPU load metric
	load = int(line[1])

	# Application start
	if not start:
		# Check when load becomes greater than 0
		if load > 0:
			start = True
	# Application stop
	else:
		# Check when load becomes 0
		if load == 0:
			window += 1
			if window == max_window:
				break
		else:
			if window > 0:
				window = 0

	# Select requested column
	if start:
		metric_sum += int(line[col])
		count += 1

if count > 0:
	avg = float(metric_sum) / float(count)
else:
	print "Warning, GPU load never greater than 0!"
	avg = 0.0

print "%s=%f" % (metrics[col], avg)
