#!/usr/bin/python
#coding=utf-8

import csv
import sys
from math import ceil

# Constants
#
min_val = 1
max_val = 255

csvfile = sys.argv[1]

#
# 0 - application name
# 1 - recipe name
# 2 - cpu pe qty
# 3 - cpu mem qty
# 4 - gpu pe qty
# 5 - cycles_avg_ms
# 6 - cpu_utiliz
# 7 - rss (KByte)
# 8 - gpu_utiliz
# 9 - cluster
#
data = list(csv.reader(open(csvfile, "rb"), delimiter=';', skipinitialspace=True))

assert len(data) > 1


# Drop CSV header
#
data.pop(0)


# Recipe header
#
print "<?xml version=\"1.0\"?>"
print "<BarbequeRTRM recipe_version=\"0.8\">"
print "\t<application priority=\"0\">"
print "\t\t<platform id=\"org.linux.cgroup\">"
print "\t\t\t<awms>"


# Sort in reverse order by metric cycles_avg_ms
#
data.sort(key=lambda x: float(x[5]), reverse=True)


# Max/mn cycles_avg_ms
#
cycles_max_ms = float(data[0][5])
cycles_min_ms = float(data[-1][5])


# Generate recipe
#
count = 0

for line in data:

	cpu_pe_qty  = int(line[2])
	cpu_mem_qty = int(line[3])
	gpu_pe_qty  = int(line[4])
	cycles_avg_ms = float(line[5])
	cpu_utiliz  = float(line[6]) * 100
	rss_mb = ceil(float(line[7]) / 1000 / 1000)
	gpu_utiliz  = float(line[8])

	val = ((cycles_max_ms-cycles_avg_ms)/(cycles_max_ms-cycles_min_ms)) * (max_val-min_val) + min_val

	awm_cpu_pe_qty  = (((int(cpu_utiliz)-1)/20+1)*20)
	awm_cpu_mem_qty = (((int(rss_mb)-1)/50+1)*50)
	awm_gpu_pe_qty  = (((int(gpu_utiliz)-1)/20+1)*20)

	if gpu_pe_qty > 0:
		# Skip bad AWMs
		if gpu_utiliz == 0:
			continue
		print "\t\t\t\t<awm id=\"%d\" name=\"gpuq%d\" value=\"%d\">" % (count, awm_gpu_pe_qty, val)
	else:
		print "\t\t\t\t<awm id=\"%d\" name=\"cpuq%d\" value=\"%d\">" % (count, awm_cpu_pe_qty, val)
	print "\t\t\t\t\t<resources>"
	print "\t\t\t\t\t\t<sys id=\"0\">"
	
	print "\t\t\t\t\t\t\t<cpu id=\"0\">"
	print "\t\t\t\t\t\t\t\t<pe qty=\"%d\"/>" % awm_cpu_pe_qty
	print "\t\t\t\t\t\t\t\t<mem units=\"Mb\" qty=\"%d\"/>" % awm_cpu_mem_qty
	print "\t\t\t\t\t\t\t</cpu>"

	if gpu_pe_qty > 0:
		print "\t\t\t\t\t\t\t<gpu id=\"0\">"
		print "\t\t\t\t\t\t\t\t<pe qty=\"%d\"/>" % awm_gpu_pe_qty
		print "\t\t\t\t\t\t\t</gpu>"

	print "\t\t\t\t\t\t</sys>"
	print "\t\t\t\t\t</resources>"
	print "\t\t\t\t</awm>"

	count += 1


# Foot
#
print "\t\t\t</awms>"
print "\t\t</platform>"
print "\t</application>"
print "</BarbequeRTRM>"
print ""
print "<!-- vim: set tabstop=4 filetype=xml : -->"
