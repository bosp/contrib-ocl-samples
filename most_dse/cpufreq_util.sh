#!/bin/bash

action=$1
gov=$2
sys_root=/sys/devices/system/cpu/ 
n_cpus=$(ls -l $sys_root | grep cpu\[0-9][0-9]* | wc -l)
n_cpus=$(( n_cpus - 1))

function set {
	echo "Setting governor $gov for $n_cpus CPUs"
	for i in `seq 0 $n_cpus`; do 
		echo $gov > /sys/devices/system/cpu/cpu$i/cpufreq/scaling_governor;
	done
}

function get {
	for i in `seq 0 $n_cpus`; do 
		cat /sys/devices/system/cpu/cpu$i/cpufreq/scaling_cur_freq;
	done
}


if [ "x$action" == "xget" ]; then
	get
elif [  "x$action" == "xset" ]; then
	set
else
	echo "ERROR: Unknown action ($action)"
fi


