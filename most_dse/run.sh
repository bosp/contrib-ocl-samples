#__MOST_GENERIC_WRAPPER__#                  INPUT_TEMPLATE_FILE           INPUT_FILE
#__MOST_GENERIC_WRAPPER__input_file__#      ../../app_cpu.recipe.in       app_cpu.recipe
#__MOST_GENERIC_WRAPPER__input_file__#      ../../app_gpu.recipe.in       app_gpu.recipe

#__MOST_GENERIC_WRAPPER__#                  METRIC_NAME        OUTPUT_FILE          TYPE         ADDITIONAL INFO
#__MOST_GENERIC_WRAPPER__output_file__#     cycles_avg_ms      app_metrics.txt      regexp       @[\w]+\:[\d]+\:perf\:cycles_avg_ms=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cpu_utiliz         app_metrics.txt      regexp       @[\w]+\:[\d]+\:perf\:cpu_utiliz=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     rss                app_metrics.txt      regexp       @[\w]+\:[\d]+\:memory\:rss=([\d]+)@
#__MOST_GENERIC_WRAPPER__output_file__#     gpu_utiliz         gpu_metrics.txt      template     load=

#!/bin/sh


################################################################################
# DSE Configuration
################################################################################

ROOT="${PWD}/../.."

BOSP_BASE="${BOSP_BASE:-$ROOT/../../../..}"
BBQUE_SYSROOT="${BBQUE_SYSROOT:-$BOSP_BASE/out}"
BIN_PATH="${BBQUE_SYSROOT}/usr/bin"

CMD="${ROOT}/run_app.sh ${BIN_PATH}"
SIMID=`basename $PWD`


APP=@__MOST_GENERIC_WRAPPER__app__@
RECIPE=@__MOST_GENERIC_WRAPPER__recipe__@
GPU_PE_QTY=@__MOST_GENERIC_WRAPPER__gpu_pe_qty__@


################################################################################
# Setup barbeque recipe
################################################################################

if [ $GPU_PE_QTY -eq 0 ]; then
	cp app_cpu.recipe ${BBQUE_SYSROOT}/etc/bbque/recipes/${RECIPE}.recipe
else
	cp app_gpu.recipe ${BBQUE_SYSROOT}/etc/bbque/recipes/${RECIPE}.recipe
fi


################################################################################
# Run application with RTLib metric report
################################################################################

export BBQUE_RTLIB_OPTS="p0:M"

# Wipe BBQ recipes
echo "Wipe BBQ recipes..."
echo "bq.am.recipes_wipe" > ${BBQUE_SYSROOT}/var/bbque/bbque_cmds

# Start GPU perf dump
echo "Start GPU perf dump..."
echo "bq.pp.ocl.pm_dump start" > ${BBQUE_SYSROOT}/var/bbque/bbque_cmds

# Clear GPU perf dump
echo "Clear GPU perf dump..."
echo "bq.pp.ocl.pm_dump clear" > ${BBQUE_SYSROOT}/var/bbque/bbque_cmds

set -e
echo "# $SIMID: $CMD"
eval "$CMD 1>/dev/null 2>app_log.txt"

# Stop GPU perf dump
echo "Stop GPU perf dump..."
echo "bq.pp.ocl.pm_dump stop" > ${BBQUE_SYSROOT}/var/bbque/bbque_cmds

# Parse application log
sed -e '1,/\.\:\: MOST statistics for AWM/d' app_log.txt > app_metrics.txt

if [ $GPU_PE_QTY -gt 0 ]; then
	# Parse GPU PlatformStatus log
	GPULOG="BBQ-PlatformStatus-GPU.dat"
	cat /tmp/BBQ-PlatformStatus-GPU[0-9].dat > ${GPULOG}
	python ${ROOT}/scripts/bbque/BBQ-PlatformStatus-parser.py -f ${GPULOG} -c 2 > gpu_metrics.txt
else
	echo "load=0" > gpu_metrics.txt
fi


################################################################################
# Clueanup
################################################################################

rm -f ${BBQUE_SYSROOT}/etc/bbque/recipes/${RECIPE}.recipe


exit 0
