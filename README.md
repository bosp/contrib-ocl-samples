# BarbequeRTRM OpenCL Samples

Example OpenCL applications integrated with BarbequeRTRM.
These samples have been taken from the AMD OpenCL APP SDK:
http://developer.amd.com/tools-and-sdks/heterogeneous-computing/amd-accelerated-parallel-processing-app-sdk/
