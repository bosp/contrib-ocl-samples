
ifdef CONFIG_CONTRIB_OCL_FLUIDSIM2D

# Targets provided by this project
.PHONY: ocl-fluidsimulation2D clean_ocl-fluidsimulation2D

# Add this to the "ocl-samples" target
ocl-samples: ocl-fluidsimulation2D
clean_ocl-samples: clean_ocl-fluidsimulation2D

MODULE_CONTRIB_OCL_FLUIDSIM2D=contrib/user/ocl-samples/FluidSimulation2D

ocl-fluidsimulation2D: ocl-sdkutil
	@echo
	@echo "==== Building FluidSimulation2DOCL ($(BUILD_TYPE)) ===="
	@[ -d $(MODULE_CONTRIB_OCL_FLUIDSIM2D)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_OCL_FLUIDSIM2D)/build/$(BUILD_TYPE) || \
		exit 1
	cd $(MODULE_CONTRIB_OCL_FLUIDSIM2D)/build/$(BUILD_TYPE) && \
		CXX=$(CXX) cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_OCL_FLUIDSIM2D)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_ocl-fluidsimulation2D:
	@echo
	@echo "==== Clean-up FluidSimulation2DOCL ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/bbque-ocl-fluidsimulation2D ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/BbqOCLFluidSimulation2D.recipe; \
		rm -f $(BUILD_DIR)/usr/bin/bbque-ocl-fluidsimulation2D; \
		rm -f $(BUILD_DIR)/usr/bin/FluidSimulation2D_Kernels.cl;
	@rm -rf $(MODULE_CONTRIB_OCL_FLUIDSIM2D)/build
	@echo

run-ocl-fluidsimulation2D: ocl-fluidsimulation2D
	@echo
	@echo "==== Running FluidSimulation2DOCL ===="
	$(MODULE_CONTRIB_OCL_FLUIDSIM2D)/run_app.sh $(BUILD_DIR)/usr/bin

else # CONFIG_CONTRIB_OCL_FLUIDSIM2D

ocl-fluidsimulation2D:
	$(warning contrib/bbque-ocl-fluidsimulation2D module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_OCL_FLUIDSIM2D

