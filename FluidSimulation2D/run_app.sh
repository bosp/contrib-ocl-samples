#!/bin/bash

PREFIX=$1

${PREFIX}/bbque-ocl-fluidsimulation2D \
--device all \
--quiet \
--iterations 100
