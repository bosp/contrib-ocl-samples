#ifndef FLUID_SIMULATION2DEXC_H
#define FLUID_SIMULATION2DEXC_H

#include <bbque/bbque_exc.h>
#include <bbque/utils/timer.h>

#include "FluidSimulation2D.hpp"

using bbque::rtlib::BbqueEXC;
using bbque::utils::Timer;


class FluidSimulation2DEXC : public BbqueEXC
{
public:
	FluidSimulation2DEXC(
		std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib);

	~FluidSimulation2DEXC();

	int parseCommandLine(int argc, char**argv);
	bool isDumpBinaryEnabled();
	int genBinaryImage();
	int cleanup();

	FluidSimulation2D * getPointer();

private:
	bool oclDeviceChanged();

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(uint8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onRelease();
	RTLIB_ExitCode_t onSuspend();

	FluidSimulation2D clFluidSim;
	Timer timer_run;

	std::string device_name;
};

#endif /* define(FLUID_SIMULATION2DEXC_H) */
