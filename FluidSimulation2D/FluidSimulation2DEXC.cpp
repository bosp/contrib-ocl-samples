#include "FluidSimulation2DEXC.hpp"

#include <cstdio>
#include <cstdlib>
#include <sstream>

#include <CL/cl.h>
#include <bbque/utils/utility.h>


/*************************************************************/
/* Setup Barbeque logging                                    */
/*************************************************************/
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "aem.fluidsimulation2D"
#undef  BBQUE_LOG_UID
#define BBQUE_LOG_UID GetUniqueID_String()


/*************************************************************/
/* Macros for logging RTLib calls                            */
/*************************************************************/

#define MSG_INF( format, ... ) \
	fprintf(stderr, FI("[INF] " format "\n"), ##__VA_ARGS__);

#define MSG_ERR( format, ... ) \
	fprintf(stderr, FE("[ERR] " format "\n"), ##__VA_ARGS__);

#define METHOD_ENTER(function) \
	fprintf(stderr, FI("[INF] %s called\n"), function)

#define METHOD_EXIT(function) \
	fprintf(stderr, FI("[INF] %s done\n"), function)



FluidSimulation2DEXC::FluidSimulation2DEXC(
		std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib) :
	BbqueEXC(name, recipe, rtlib),
	clFluidSim("FluidSimulation2D")
{
	if (clFluidSim.initialize() != SDK_SUCCESS)
	{
		MSG_ERR("Method clFluidSim_initialize failed");
		exit(EXIT_FAILURE);
	}
}

FluidSimulation2DEXC::~FluidSimulation2DEXC()
{

}

int FluidSimulation2DEXC::parseCommandLine(int argc, char**argv)
{
	if (clFluidSim.parseCommandLine(argc,argv) != SDK_SUCCESS)
	{
		MSG_ERR("Method clFluidSim_parseCommandLine failed");
		return SDK_FAILURE;
	}
	return SDK_SUCCESS;
}

bool FluidSimulation2DEXC::isDumpBinaryEnabled()
{
	return clFluidSim.isDumpBinaryEnabled();
}

int FluidSimulation2DEXC::genBinaryImage()
{
	if (clFluidSim.genBinaryImage() != SDK_SUCCESS)
	{
		MSG_ERR("Method clFluidSim_genBinaryImage failed");
		return SDK_FAILURE;
	}
	return SDK_SUCCESS;
}

int FluidSimulation2DEXC::cleanup()
{
	if(clFluidSim.cleanup() != SDK_SUCCESS)
	{
		MSG_ERR("Method clFluidSim_cleanup failed");
		return SDK_FAILURE;
	}
	return SDK_SUCCESS;
}

FluidSimulation2D * FluidSimulation2DEXC::getPointer()
{
	return &clFluidSim;
}

RTLIB_ExitCode_t FluidSimulation2DEXC::onSetup()
{
	METHOD_ENTER("onSetup");
	if (clFluidSim.getAwm() >= 0) {
		RTLIB_Constraint_t constr_set = {
			clFluidSim.getAwm(),
			CONSTRAINT_ADD,
			UPPER_BOUND
		};
		SetConstraints(&constr_set, 1);
	}
	METHOD_EXIT("onSetup");
	return RTLIB_OK;
}

bool FluidSimulation2DEXC::oclDeviceChanged()
{
	bool ret = false;

	char *device_name_str = NULL;
	size_t device_name_size = 0;

	cl_int status = 0;
	cl_device_id devId;
	cl_platform_id pltId;

	status = clGetPlatformIDs(1, &pltId, NULL);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetPlatformIDs failed");
		exit(EXIT_FAILURE);
	}

	status = clGetDeviceIDs(pltId, CL_DEVICE_TYPE_ALL, 1, &devId, NULL);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetDeviceIDs failed");
		exit(EXIT_FAILURE);
	}

	status = clGetDeviceInfo(devId, CL_DEVICE_NAME, 0, NULL, &device_name_size);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetDeviceInfo failed");
		exit(EXIT_FAILURE);
	}

	device_name_str = (char *)malloc(device_name_size+1);
	if (device_name_str == NULL) {
		MSG_ERR("Memory allocation failed");
		exit(EXIT_FAILURE);
	}

	status = clGetDeviceInfo(devId, CL_DEVICE_NAME, device_name_size, device_name_str, &device_name_size);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetDeviceInfo failed");
		exit(EXIT_FAILURE);
	}

	device_name_str[device_name_size] = '\0';
	if (device_name.compare(device_name_str) != 0) {
		device_name = device_name_str;
		ret = true;
	}

	free ((void *)device_name_str);

	return ret;
}

RTLIB_ExitCode_t FluidSimulation2DEXC::onConfigure(uint8_t awm_id)
{
	METHOD_ENTER("onConfigure");
	timer_run.start();

	if (!oclDeviceChanged()) {
		MSG_INF("Reconfiguring quota on the same device [%s]", device_name.c_str());
		return RTLIB_OK;
	}
	MSG_INF("OpenCL device has changed [%s]", device_name.c_str());

	if (Cycles() > 0) {
		if (clFluidSim.cleanup() != SDK_SUCCESS) {
			MSG_ERR("Method clFluidSim_cleanup failed");
			return RTLIB_ERROR;
		}
	}

	if (clFluidSim.setup() != SDK_SUCCESS)
	{
		MSG_ERR("Method clFluidSim_setup failed");
		return RTLIB_ERROR;
	}

	if (clFluidSim.setupCL() != SDK_SUCCESS)
	{
		MSG_ERR("Method clFluidSim_setupCL failed");
		return RTLIB_ERROR;
	}

	METHOD_EXIT("onConfigure");
	timer_run.stop();
	double onConfigure_time = timer_run.getElapsedTimeUs();
	MSG_INF("onConfigure_time %f", onConfigure_time);

	return RTLIB_OK;
}

RTLIB_ExitCode_t FluidSimulation2DEXC::onRun()
{
	if ( Cycles() >= clFluidSim.getIterations() )
	{
		return RTLIB_EXC_WORKLOAD_NONE;
	}

	METHOD_ENTER("onRun");
	timer_run.start();

	if (clFluidSim.run() != SDK_SUCCESS)
	{
		Terminate();
		return RTLIB_ERROR;
	}
	METHOD_EXIT("onRun");

	timer_run.stop();
	double onRun_time = timer_run.getElapsedTimeUs();
	MSG_INF("onRun_time %f", onRun_time);

	return RTLIB_OK;
}

RTLIB_ExitCode_t FluidSimulation2DEXC::onMonitor()
{
	return RTLIB_OK;
}

RTLIB_ExitCode_t FluidSimulation2DEXC::onRelease()
{
	METHOD_ENTER("onRelease");

	if (clFluidSim.verifyResults()!=SDK_SUCCESS)
	{
		return RTLIB_ERROR;
	}

//	if (clFluidSim.cleanup() != SDK_SUCCESS)
//	{
//		MSG_ERR("Method clFluidSim_cleanup failed");
//		return RTLIB_ERROR;
//	}

	METHOD_EXIT("onRelease");

	return RTLIB_OK;
}

RTLIB_ExitCode_t FluidSimulation2DEXC::onSuspend()
{
	METHOD_ENTER("onSuspend");

	fprintf(stderr, "\n");
	fprintf(stderr, "*** Execution context suspended ***\n");
	fprintf(stderr, "\n");

	METHOD_EXIT("onSuspend");

	return RTLIB_OK;
}

