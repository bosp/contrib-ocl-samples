
ifdef CONFIG_CONTRIB_OCL_SAMPLES

# Targets provided by this project
.PHONY: ocl-samples clean_ocl-samples

# Add this to the "contrib_testing" target
contrib: ocl-samples
clean_contrib: clean_ocl-samples

ocl-samples:
	@echo "All enabled OpenCL samples build"

clean_ocl-samples:
	@echo "All enabled OpenCL samples cleaned up"

else # CONFIG_CONTRIB_OCL_SAMPLES

ocl-samples:
	$(warning contrib/bbque-ocl-samples module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_OCL_SAMPLES

