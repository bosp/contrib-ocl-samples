#include "MonteCarloAsianEXC.hpp"

#include <cstdio>
#include <cstdlib>
#include <sstream>

#include <CL/cl.h>
#include <bbque/utils/utility.h>

#define APP_NAME "montecarloDP"
#define APP_RECIPE "BbqOCLMonteCarloAsianDP"


/*************************************************************/
/* Setup Barbeque logging                                    */
/*************************************************************/
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "aem.montecarlo"
#undef  BBQUE_LOG_UID
#define BBQUE_LOG_UID GetUniqueID_String()


/*************************************************************/
/* Macros for logging RTLib calls                            */
/*************************************************************/

#define MSG_INF( format, ... ) \
	fprintf(stderr, FI("[INF] " format "\n"), ##__VA_ARGS__);

#define MSG_ERR( format, ... ) \
	fprintf(stderr, FE("[ERR] " format "\n"), ##__VA_ARGS__);

#define METHOD_ENTER(function) \
	fprintf(stderr, FI("[INFO] %s called\n"), function)

#define METHOD_EXIT(function) \
	fprintf(stderr, FI("[INFO] %s done\n"), function)



MonteCarloAsianEXC::MonteCarloAsianEXC(std::string const & name, std::string const & recipe,
		RTLIB_Services_t *rtlib) :
	BbqueEXC(name, recipe, rtlib),
	clMonteCarloAsianDP("OpenCL Monte Carlo simulation for Asian Option Pricing")
{
	if (clMonteCarloAsianDP.initialize() != SDK_SUCCESS)
	{
		MSG_ERR("Method clMonteCarloAsianDP_initialize failed");
		exit(EXIT_FAILURE);
	}
}

MonteCarloAsianEXC::~MonteCarloAsianEXC()
{

}

int MonteCarloAsianEXC::parseCommandLine(int argc, char *argv[])
{
	if (clMonteCarloAsianDP.parseCommandLine(argc, argv) != SDK_SUCCESS)
	{
		MSG_ERR("Method clMonteCarloAsianDP_parseCommandLine failed");
		return SDK_FAILURE;
	}
	return SDK_SUCCESS;
}

bool MonteCarloAsianEXC::isDumpBinaryEnabled()
{
	return clMonteCarloAsianDP.isDumpBinaryEnabled();
}

int MonteCarloAsianEXC::genBinaryImage()
{
	if (clMonteCarloAsianDP.genBinaryImage() != SDK_SUCCESS)
	{
		MSG_ERR("Method clMonteCarloAsianDP_genBinaryImage failed");
		return SDK_FAILURE;
	}
	return SDK_SUCCESS;
}

RTLIB_ExitCode_t MonteCarloAsianEXC::onSetup()
{
	METHOD_ENTER("onSetup");


	METHOD_EXIT("onSetup");
	return RTLIB_OK;
}

bool MonteCarloAsianEXC::oclDeviceChanged()
{
	bool ret = false;

	char *device_name_str = NULL;
	size_t device_name_size = 0;

	cl_int status = 0;
	cl_device_id devId;
	cl_platform_id pltId;

	status = clGetPlatformIDs(1, &pltId, NULL);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetPlatformIDs failed");
		exit(EXIT_FAILURE);
	}

	status = clGetDeviceIDs(pltId, CL_DEVICE_TYPE_ALL, 1, &devId, NULL);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetDeviceIDs failed");
		exit(EXIT_FAILURE);
	}

	status = clGetDeviceInfo(devId, CL_DEVICE_NAME, 0, NULL, &device_name_size);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetDeviceInfo failed");
		exit(EXIT_FAILURE);
	}

	device_name_str = (char *)malloc(device_name_size+1);
	if (device_name_str == NULL) {
		MSG_ERR("Memory allocation failed");
		exit(EXIT_FAILURE);
	}

	status = clGetDeviceInfo(devId, CL_DEVICE_NAME, device_name_size, device_name_str, &device_name_size);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetDeviceInfo failed");
		exit(EXIT_FAILURE);
	}

	device_name_str[device_name_size] = '\0';
	if (device_name.compare(device_name_str) != 0) {
		device_name = device_name_str;
		ret = true;
	}

	free ((void *)device_name_str);

	return ret;
}

RTLIB_ExitCode_t MonteCarloAsianEXC::onConfigure(uint8_t awm_id)
{
	METHOD_ENTER("onConfigure");

	if (!oclDeviceChanged()) {
		MSG_INF("Reconfiguring quota on the same device [%s]", device_name.c_str());
		return RTLIB_OK;
	}
	MSG_INF("OpenCL device has changed [%s]", device_name.c_str());

	if (Cycles() > 0) {
		if (clMonteCarloAsianDP.cleanup() != SDK_SUCCESS) {
			MSG_ERR("Method clMonteCarloAsianDP_cleanup failed");
			return RTLIB_ERROR;
		}
	}

	if (clMonteCarloAsianDP.setup() != SDK_SUCCESS)
	{
		MSG_ERR("Method clMonteCarloAsianDP_setup failed");
		return RTLIB_ERROR;
	}

	if (clMonteCarloAsianDP.setupCL() != SDK_SUCCESS)
	{
		MSG_ERR("Method clMonteCarloAsianDP_setup failed");
		return RTLIB_ERROR;
	}

	METHOD_EXIT("onConfigure");
	return RTLIB_OK;
}

RTLIB_ExitCode_t MonteCarloAsianEXC::onRun()
{
	if ( Cycles() >= clMonteCarloAsianDP.getIterations() )
	{
		return RTLIB_EXC_WORKLOAD_NONE;
	}

	METHOD_ENTER("onRun");
	if (clMonteCarloAsianDP.run() != SDK_SUCCESS)
	{
		MSG_ERR("Method clMonteCarloAsianDP_run failed");
		Terminate();
		return RTLIB_ERROR;
	}
	METHOD_EXIT("onRun");

	return RTLIB_OK;
}

RTLIB_ExitCode_t MonteCarloAsianEXC::onMonitor()
{
	return RTLIB_OK;
}

RTLIB_ExitCode_t MonteCarloAsianEXC::onRelease()
{
	METHOD_ENTER("onRelease");

	if (clMonteCarloAsianDP.verifyResults()!=SDK_SUCCESS)
	{
		MSG_ERR("Method clMonteCarloAsianDP_verifyResults failed");
		return RTLIB_ERROR;
	}

	if (clMonteCarloAsianDP.cleanup() != SDK_SUCCESS)
	{
		MSG_ERR("Method clMonteCarloAsianDP_cleanup failed");
		return RTLIB_ERROR;
	}

	METHOD_EXIT("onRelease");

	return RTLIB_OK;
}

RTLIB_ExitCode_t MonteCarloAsianEXC::onSuspend()
{
	METHOD_ENTER("onSuspend");

	fprintf(stderr, "\n");
	fprintf(stderr, "*** Execution context suspended ***\n");
	fprintf(stderr, "\n");

	METHOD_EXIT("onSuspend");

	return RTLIB_OK;
}


int main(int argc, char *argv[])
{
	RTLIB_ExitCode_t result;
	RTLIB_Services_t * rtlib;

	// First step: initialize the communication with Barbeque RTRM
	RTLIB_Init(APP_NAME, &rtlib);
	if (!rtlib) {
		fprintf(stderr, "Error: Cannot find Barbeque RTRM running\n");
		return 1;
	}

	// The EXC;
	printf(" ... Registering a new Execution Context ... \n");
	MonteCarloAsianEXC *montecarloExcPtr = new MonteCarloAsianEXC(APP_NAME, APP_RECIPE, rtlib);
	if (!montecarloExcPtr) {
		fprintf(stderr, "Error: Unable to register the EXC\n");
		return 1;
	}

	// Parse command line options
	if (montecarloExcPtr->parseCommandLine(argc, argv) != SDK_SUCCESS)
	{
		return 1;
	}

	// Check if only kernel binaries are needed
	if (montecarloExcPtr->isDumpBinaryEnabled())
	{
		return montecarloExcPtr->genBinaryImage();
	}

	// Let's start!
	printf(" ... Starting the EXC ... \n");
	result = montecarloExcPtr->Start();
	if (result != RTLIB_OK) {
		fprintf(stderr, "Error: Unable to start the EXC\n");
		return 1;
	}

	// Wait for completion of the EXC
	montecarloExcPtr->WaitCompletion();
	printf(" ... The EXC finished ... \n");

	// Release Execution Context object
	delete montecarloExcPtr;

	printf("Exit program.\n");

	return EXIT_SUCCESS;
}

