#include <cstdio>
#include <cstdlib>

#include "MonteCarloAsianDP.hpp"
#include "MonteCarloAsianEXC.hpp"


/*************************************************************/
/* Use these values if not specified from command line       */
/*************************************************************/
#define DEFAULT_AWM_ID   0

/*************************************************************/
/* Macro for MOST errors                                     */
/*************************************************************/
#define MOST_ERROR(msg) printf("@error=%s@\n", msg)

/*************************************************************/
/* Global variables                                          */
/*************************************************************/
MonteCarloAsianDP* me;       /**< Pointing to MonteCarloAsian class */


int main(int argc, char *argv[])
{
	int i;
	std::string recipe_name;
	RTLIB_ExitCode_t result;
	RTLIB_Services_t * rtlib;

	uint8_t awm_id = DEFAULT_AWM_ID;

	const char app_name[] = "MonteCarloAsianOCL";
	const char exc_name[] = "MonteCarloAsianOCL";

	bool awm_found = false;
	// Retrieve awm_id from command-line parameters
	for (i=0; i<argc && !awm_found; i++)
	{
		if ( strcmp(argv[i], "--awm_id") == 0 )
		{
			awm_id = (uint8_t)atoi( argv[i+1] );
			awm_found = true;
		}
	}
	if (awm_found)
	{
		for (i=i-1; i+2 < argc; i++)
		{
			argv[i] = argv[i+2];
		}
		argc -= 2;
	}

	// If AWM_ID is passed as command line parameter
	// then set a constraint on the AWM
	if (awm_found)
		recipe_name = "BbqMostOclDse";
	else
		recipe_name = "BbqOCLMonteCarloAsianDP";

	// First step: initialize the communication with Barbeque RTRM
	RTLIB_Init(app_name, &rtlib);
	if (!rtlib) {
		fprintf(stderr, "Error: Cannot find Barbeque RTRM running\n");
		MOST_ERROR("RTLIB_Init");
		return EXIT_FAILURE;
	}

	// The EXC;
	printf(" ... Registering a new Execution Context ... \n");
	MonteCarloAsianEXC *montecarloExcPtr = new MonteCarloAsianEXC(exc_name, recipe_name, rtlib,
		awm_found, awm_id);
	if (!montecarloExcPtr) {
		fprintf(stderr, "Error: Unable to register the EXC\n");
		MOST_ERROR("ExC_constructor");
		return EXIT_FAILURE;
	}

	// Parse command line options
	if (montecarloExcPtr->parseCommandLine(argc, argv) != SDK_SUCCESS)
	{
		return EXIT_FAILURE;
	}

	// Check if only kernel binaries are needed
	if (montecarloExcPtr->isDumpBinaryEnabled())
	{
		return montecarloExcPtr->genBinaryImage();
	}

	// Let's start!
	printf(" ... Starting the EXC ... \n");
	result = montecarloExcPtr->Start();
	if (result != RTLIB_OK) {
		fprintf(stderr, "Error: Unable to start the EXC\n");
		MOST_ERROR("ExC_start");
		return EXIT_FAILURE;
	}

	// Wait for completion of the EXC
	montecarloExcPtr->WaitCompletion();
	printf(" ... The EXC finished ... \n");

	// Cleanup
	if (montecarloExcPtr->cleanup() != SDK_SUCCESS)
		return SDK_FAILURE;

	// Release Execution Context object
	delete montecarloExcPtr;

	printf("Exit program.\n");

	return EXIT_SUCCESS;
}

