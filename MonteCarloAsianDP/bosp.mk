
ifdef CONFIG_CONTRIB_OCL_MONTECARLOASIAN_DP

# Targets provided by this project
.PHONY: ocl-montecarlo clean_ocl-montecarlo

# Add this to the "ocl-samples" target
ocl-samples: ocl-montecarlo
clean_ocl-samples: clean_ocl-montecarlo

MODULE_CONTRIB_OCL_MONTECARLOASIAN_DP=contrib/user/ocl-samples/MonteCarloAsianDP

ocl-montecarlo: ocl-sdkutil
	@echo
	@echo "==== Building MonteCarloAsianOCL ($(BUILD_TYPE)) ===="
	@[ -d $(MODULE_CONTRIB_OCL_MONTECARLOASIAN_DP)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_OCL_MONTECARLOASIAN_DP)/build/$(BUILD_TYPE) || \
		exit 1
	cd $(MODULE_CONTRIB_OCL_MONTECARLOASIAN_DP)/build/$(BUILD_TYPE) && \
		CXX=$(CXX) cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_OCL_MONTECARLOASIAN_DP)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_ocl-montecarlo:
	@echo
	@echo "==== Clean-up MonteCarloAsianOCL ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/bbque-ocl-montecarlo ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/BbqOCLMonteCarloAsianDP.recipe; \
		rm -f $(BUILD_DIR)/usr/bin/bbque-ocl-montecarlo; \
		rm -f $(BUILD_DIR)/usr/bin/MonteCarloAsianDP_Kernels.cl;
	@rm -rf $(MODULE_CONTRIB_OCL_MONTECARLOASIAN_DP)/build
	@echo

run-ocl-montecarlo: ocl-montecarlo
	@echo
	@echo "==== Running MonteCarloAsianOCL ===="
	$(MODULE_CONTRIB_OCL_MONTECARLOASIAN_DP)/run_app.sh $(BUILD_DIR)/usr/bin

else # CONFIG_CONTRIB_OCL_MONTECARLOASIAN_DP

ocl-montecarlo:
	$(warning contrib/bbque-ocl-montecarlo module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_OCL_MONTECARLOASIAN_DP

