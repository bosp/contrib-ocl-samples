#ifndef MONTECARLOASIAN_EXC_H
#define MONTECARLOASIAN_EXC_H

#include <bbque/bbque_exc.h>

#include "MonteCarloAsianDP.hpp"

using bbque::rtlib::BbqueEXC;


class MonteCarloAsianEXC : public BbqueEXC
{
public:
	MonteCarloAsianEXC(std::string const & name, std::string const & recipe,
		RTLIB_Services_t *rtlib);
	~MonteCarloAsianEXC();

	int parseCommandLine(int argc, char *argv[]);
	bool isDumpBinaryEnabled();
	int genBinaryImage();
	int cleanup();

private:
	bool oclDeviceChanged();

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(uint8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onRelease();
	RTLIB_ExitCode_t onSuspend();

	MonteCarloAsianDP clMonteCarloAsianDP;

	std::string device_name;
};

#endif /* define(MONTECARLOASIAN_EXC_H) */
