#!/bin/bash

PREFIX=$1

${PREFIX}/bbque-ocl-montecarlo \
--device all \
--quiet \
--steps 4 \
--iterations 10
