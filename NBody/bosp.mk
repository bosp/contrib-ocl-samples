
ifdef CONFIG_CONTRIB_OCL_NBODY

# Targets provided by this project
.PHONY: ocl-nbody clean_ocl-nbody

# Add this to the "ocl-samples" target
ocl-samples: ocl-nbody
clean_ocl-samples: clean_ocl-nbody

MODULE_CONTRIB_OCL_NBODY=contrib/user/ocl-samples/NBody

ocl-nbody: ocl-sdkutil
	@echo
	@echo "==== Building NBodyOCL ($(BUILD_TYPE)) ===="
	@[ -d $(MODULE_CONTRIB_OCL_NBODY)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_OCL_NBODY)/build/$(BUILD_TYPE) || \
		exit 1
	cd $(MODULE_CONTRIB_OCL_NBODY)/build/$(BUILD_TYPE) && \
		CXX=$(CXX) cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_OCL_NBODY)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_ocl-nbody:
	@echo
	@echo "==== Clean-up NBodyOCL ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/bbque-ocl-nbody ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/BbqOCLNBody.recipe; \
		rm -f $(BUILD_DIR)/usr/bin/bbque-ocl-nbody; \
		rm -f $(BUILD_DIR)/usr/bin/NBody_Kernels.cl;
	@rm -rf $(MODULE_CONTRIB_OCL_NBODY)/build
	@echo

run-ocl-nbody: ocl-nbody
	@echo
	@echo "==== Running NBodyOCL ===="
	$(MODULE_CONTRIB_OCL_NBODY)/run_app.sh $(BUILD_DIR)/usr/bin

else # CONFIG_CONTRIB_OCL_NBODY

ocl-nbody:
	$(warning contrib/bbque-ocl-nbody module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_OCL_NBODY

