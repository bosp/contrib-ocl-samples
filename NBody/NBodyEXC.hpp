#ifndef NBODY_EXC_H
#define NBODY_EXC_H

#include <bbque/bbque_exc.h>

#include "NBody.hpp"

using bbque::rtlib::BbqueEXC;


class NBodyEXC : public BbqueEXC
{
public:
	NBodyEXC(std::string const & name, std::string const & recipe,
		RTLIB_Services_t *rtlib);
	~NBodyEXC();

	int parseCommandLine(int argc, char *argv[]);
	bool isDumpBinaryEnabled();
	int genBinaryImage();
	int cleanup();

	NBody * getPointer();

private:
	bool oclDeviceChanged();

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(uint8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onRelease();
	RTLIB_ExitCode_t onSuspend();

	NBody clNBody;

	std::string device_name;
};

#endif /* define(NBODY_EXC_H) */
