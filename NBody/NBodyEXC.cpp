#include "NBodyEXC.hpp"

#include <cstdio>
#include <cstdlib>
#include <sstream>

#include <CL/cl.h>
#include <bbque/utils/utility.h>

/*************************************************************/
/* Setup Barbeque logging                                    */
/*************************************************************/
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "aem.nbody"
#undef  BBQUE_LOG_UID
#define BBQUE_LOG_UID GetUniqueID_String()


/*************************************************************/
/* Macros for logging RTLib calls                            */
/*************************************************************/

#define MSG_INF( format, ... ) \
	fprintf(stderr, FI("[INF] " format "\n"), ##__VA_ARGS__);

#define MSG_ERR( format, ... ) \
	fprintf(stderr, FE("[ERR] " format "\n"), ##__VA_ARGS__);

#define METHOD_ENTER(function) \
	fprintf(stderr, FI("[INFO] %s called\n"), function)

#define METHOD_EXIT(function) \
	fprintf(stderr, FI("[INFO] %s done\n"), function)



NBodyEXC::NBodyEXC(std::string const & name, std::string const & recipe,
		RTLIB_Services_t *rtlib) :
	BbqueEXC(name, recipe, rtlib),
	clNBody("OpenCL NBody")
{
	if (clNBody.initialize() != SDK_SUCCESS)
	{
		MSG_ERR("Method clNBody_initialize failed");
		exit(EXIT_FAILURE);
	}
}

NBodyEXC::~NBodyEXC()
{

}

int NBodyEXC::parseCommandLine(int argc, char *argv[])
{
	if (clNBody.parseCommandLine(argc, argv) != SDK_SUCCESS)
	{
		MSG_ERR("Method clNBody_parseCommandLine failed");
		return SDK_FAILURE;
	}
	return SDK_SUCCESS;
}

bool NBodyEXC::isDumpBinaryEnabled()
{
	return clNBody.isDumpBinaryEnabled();
}

int NBodyEXC::genBinaryImage()
{
	if (clNBody.genBinaryImage() != SDK_SUCCESS)
	{
		MSG_ERR("Method clNBody_genBinaryImage failed");
		return SDK_FAILURE;
	}
	return SDK_SUCCESS;
}

int NBodyEXC::cleanup()
{
	if (clNBody.cleanup() != SDK_SUCCESS)
	{
		MSG_ERR("Method clNBody_cleanup failed");
		return SDK_FAILURE;
	}
	return SDK_SUCCESS;
}

NBody *NBodyEXC::getPointer()
{
	return &clNBody;
}

RTLIB_ExitCode_t NBodyEXC::onSetup()
{
	METHOD_ENTER("onSetup");


	METHOD_EXIT("onSetup");
	return RTLIB_OK;
}

bool NBodyEXC::oclDeviceChanged()
{
	bool ret = false;

	char *device_name_str = NULL;
	size_t device_name_size = 0;

	cl_int status = 0;
	cl_device_id devId;
	cl_platform_id pltId;

	status = clGetPlatformIDs(1, &pltId, NULL);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetPlatformIDs failed");
		exit(EXIT_FAILURE);
	}

	status = clGetDeviceIDs(pltId, CL_DEVICE_TYPE_ALL, 1, &devId, NULL);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetDeviceIDs failed");
		exit(EXIT_FAILURE);
	}

	status = clGetDeviceInfo(devId, CL_DEVICE_NAME, 0, NULL, &device_name_size);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetDeviceInfo failed");
		exit(EXIT_FAILURE);
	}

	device_name_str = (char *)malloc(device_name_size+1);
	if (device_name_str == NULL) {
		MSG_ERR("Memory allocation failed");
		exit(EXIT_FAILURE);
	}

	status = clGetDeviceInfo(devId, CL_DEVICE_NAME, device_name_size, device_name_str, &device_name_size);
	if (status != CL_SUCCESS) {
		MSG_ERR("Method clGetDeviceInfo failed");
		exit(EXIT_FAILURE);
	}

	device_name_str[device_name_size] = '\0';
	if (device_name.compare(device_name_str) != 0) {
		device_name = device_name_str;
		ret = true;
	}

	free ((void *)device_name_str);

	return ret;
}

RTLIB_ExitCode_t NBodyEXC::onConfigure(uint8_t awm_id)
{
	METHOD_ENTER("onConfigure");

	if (!oclDeviceChanged()) {
		MSG_INF("Reconfiguring quota on the same device [%s]", device_name.c_str());
		return RTLIB_OK;
	}
	MSG_INF("OpenCL device has changed [%s]", device_name.c_str());

	if (Cycles() > 0) {
		if (clNBody.cleanup() != SDK_SUCCESS) {
			MSG_ERR("Method clNBody_cleanup failed");
			return RTLIB_ERROR;
		}
	}

	if (clNBody.setup() != SDK_SUCCESS)
	{
		MSG_ERR("Method clNBody_setup failed");
		return RTLIB_ERROR;
	}

	// Setup OpenCL kernels
	if (clNBody.setupCLKernels() != SDK_SUCCESS)
	{
		MSG_ERR("Method clNBody_setupCLKernels failed");
		return RTLIB_ERROR;
	}

	METHOD_EXIT("onConfigure");
	return RTLIB_OK;
}

RTLIB_ExitCode_t NBodyEXC::onRun()
{
	if ( Cycles() >= clNBody.getIterations() )
	{
		return RTLIB_EXC_WORKLOAD_NONE;
	}

	METHOD_ENTER("onRun");
	if (clNBody.runCLKernels() != SDK_SUCCESS)
	{
		MSG_ERR("Method clNBody_runCLKernels failed");
		Terminate();
		return RTLIB_ERROR;
	}
	METHOD_EXIT("onRun");

	return RTLIB_OK;
}

RTLIB_ExitCode_t NBodyEXC::onMonitor()
{
	return RTLIB_OK;
}

RTLIB_ExitCode_t NBodyEXC::onRelease()
{
	METHOD_ENTER("onRelease");

	if (clNBody.verifyResults()!=SDK_SUCCESS)
	{
		MSG_ERR("Method clNBody_verifyResults failed");
		return RTLIB_ERROR;
	}

	METHOD_EXIT("onRelease");

	return RTLIB_OK;
}

RTLIB_ExitCode_t NBodyEXC::onSuspend()
{
	METHOD_ENTER("onSuspend");

	fprintf(stderr, "\n");
	fprintf(stderr, "*** Execution context suspended ***\n");
	fprintf(stderr, "\n");

	METHOD_EXIT("onSuspend");

	return RTLIB_OK;
}

