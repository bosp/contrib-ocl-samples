#!/bin/bash

PREFIX=$1

${PREFIX}/bbque-ocl-nbody \
--device all \
--quiet \
--particles 32768 \
--iterations 1000
